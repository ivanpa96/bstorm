import { ThrottlerModule } from "@nestjs/throttler";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const config: TypeOrmModuleOptions = {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: +process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    autoLoadEntities: true,
    synchronize: true,
    entities: ['dist/**/*.entity.js'],
    migrations: ['src/migration/*.js'],
    cli: {
        migrationsDir: 'src/migration'
    },
}

export const throttleConfig: ThrottlerModule = {
    ttl: 60,
    limit: 1
}

module.exports = config;
module.exports = throttleConfig;