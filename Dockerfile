FROM node:12.13-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install 

COPY . .

RUN npm run build

EXPOSE 3000

CMD ["npm","run","start:dev"]
# CMD["node","dist/main"]