CREATE TABLE "user" (
  "id" uuid PRIMARY KEY,
  "firstName" varchar,
  "lastName" varchar,
  "imageUrl" varchar,
  "email" varchar
);

CREATE TABLE "access_token" (
  "id" uuid PRIMARY KEY,
  "revoked" boolean,
  "expires_at" datetime,
  "created_at" datetime,
  "updated_at" datetime,
  "user_id" uuid
);

CREATE TABLE "refresh_token" (
  "id" uuid PRIMARY KEY,
  "revoked" boolean,
  "expires_at" datetime,
  "created_at" datetime,
  "updated_at" datetime,
  "access_token_id" uuid
);

CREATE TABLE "category" (
  "id" uuid PRIMARY KEY,
  "title" varchar,
  "created_at" datetime,
  "updated_at" datetime
);

CREATE TABLE "user_category" (
  "id" uuid PRIMARY KEY,
  "categoryId" uuid,
  "userId" uuid,
  "created_at" datetime,
  "updated_at" datetime
);

CREATE TABLE "post" (
  "id" uuid PRIMARY KEY,
  "title" varchar,
  "description" varchar,
  "image" varchar,
  "total_likes" numeric,
  "total_comments" numeric,
  "created_at" datetime,
  "updated_at" datetime,
  "user_id" uuid,
  "category_id" uuid
);

CREATE TABLE "like" (
  "id" uuid PRIMARY KEY,
  "count" numeric,
  "created_at" datetime,
  "updated_at" datetime,
  "user_liked_id" uuid,
  "post_id" uuid
);

CREATE TABLE "comment" (
  "id" uuid PRIMARY KEY,
  "text" varchar,
  "created_at" datetime,
  "updated_at" datetime,
  "user_commented_id" uuid,
  "post_id" uuid
);

ALTER TABLE "access_token" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "refresh_token" ADD FOREIGN KEY ("access_token_id") REFERENCES "access_token" ("id");

ALTER TABLE "user_category" ADD FOREIGN KEY ("categoryId") REFERENCES "category" ("id");

ALTER TABLE "user_category" ADD FOREIGN KEY ("userId") REFERENCES "user" ("id");

ALTER TABLE "post" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "post" ADD FOREIGN KEY ("category_id") REFERENCES "category" ("id");

ALTER TABLE "like" ADD FOREIGN KEY ("post_id") REFERENCES "post" ("id");

ALTER TABLE "like" ADD FOREIGN KEY ("user_liked_id") REFERENCES "user" ("id");

ALTER TABLE "comment" ADD FOREIGN KEY ("post_id") REFERENCES "post" ("id");

ALTER TABLE "comment" ADD FOREIGN KEY ("user_commented_id") REFERENCES "user" ("id");
