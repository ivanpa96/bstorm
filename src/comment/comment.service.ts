import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from 'src/models/comment.entity';
import { Post } from 'src/models/post.entity';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CommentService {
    @InjectRepository(Comment)
    commentRepository: Repository<Comment>;

    /**
     * Find comment by user and post
     * @param post 
     * @param user 
     * @returns Comment
     */
    async findComment(post: Post, user: User): Promise<Comment> {

        return await this.commentRepository.findOne({
            where: {
                user_commented: user,
                post: post
            }
        })
    }

    /**
     * Save new comment
     * @param comment 
     * @returns Comment
     */
    async saveComment(comment: Comment): Promise<Comment> {
        return await this.commentRepository.save(comment);
    }
}
