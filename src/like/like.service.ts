import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like } from 'src/models/like.entity';
import { Post } from 'src/models/post.entity';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LikeService {
    @InjectRepository(Like)
    likeRepository: Repository<Like>;

    /**
     * Find like by user and post
     * @param post 
     * @param user 
     * @returns Like
     */
    async findLike(post: Post, user: User): Promise<Like> {

        return await this.likeRepository.findOne({
            where: {
                user_liked: user,
                post: post
            }
        })
    }

    /**
     * Save new like
     * @param like 
     * @returns Like
     */
    async saveLike(like: Like): Promise<Like> {
        console.log(like)
        return await this.likeRepository.save(like);
    }
}
