import { Module } from '@nestjs/common';
import { LikeService } from './like.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Like } from 'src/models/like.entity';

@Module({
  imports: [TypeOrmModule.forFeature([
    Like
  ])
  ],
  providers: [LikeService]
})
export class LikeModule { }
