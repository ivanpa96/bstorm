import { diskStorage } from 'multer';
import path from 'path';
import { uuid } from 'uuidv4';

export const storage = {
    storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
            const filename: string = uuid();
            const extension: string = file.originalname;

            cb(null, `${filename}${extension}`)
        }
    })
}