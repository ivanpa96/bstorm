import { UnprocessableEntityException } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { throws } from 'assert';
import { UpdateUserCategoryDto } from 'src/dto/update-user-category.dto';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { UserCategory } from '../models/user-category.entity';

@Injectable()
export class UserCategoryService {
    @InjectRepository(UserCategory)
    userCategoyRepository: Repository<UserCategory>;


    /**
     * Save user's categories
     * @param userCategory 
     * @returns Array of user's categories
     */
    async createUserCategory(userCategory: UserCategory[]): Promise<UserCategory[]> {
        return await this.userCategoyRepository.save(userCategory);
    }

    /**
     * Get user's categories from database
     * @param user_id 
     * @returns Array of user's categories
     */
    async getUserCategories(user_id: string): Promise<UserCategory[]> {
        return await this.userCategoyRepository.find({
            where: {
                userId: user_id
            }
        });
    }

    /**
     * Update user's categories
     * @param userCategory 
     * @param user 
     * @returns Array of user's categories
     */
    async updateUserCategory(userCategory: UpdateUserCategoryDto[], user: User): Promise<UserCategory[]> {
        var forInsert: UserCategory[] = [];
        for (let i = 0; i < userCategory.length - 1; i++) {
            for (let j = i + 1; j < userCategory.length; j++) {
                if (userCategory[i].categoryId === userCategory[j].categoryId) {
                    throw new UnprocessableEntityException('Cannot enter the same category more than once!');
                }
            }
        }
        const userCategories = await this.getUserCategories(user.id);
        userCategory.forEach(element => {
            const uc = new UserCategory(user.id, element.categoryId.toString());
            forInsert.push(uc);
        });
        if (userCategory.length < 3) {
            throw new UnprocessableEntityException('Select three or more categories!');
        }
        await this.userCategoyRepository.remove(userCategories)


        return await this.userCategoyRepository.save(forInsert);
    }
}
