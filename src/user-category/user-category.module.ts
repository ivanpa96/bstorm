import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserCategory } from 'src/models/user-category.entity';
import { UserCategoryService } from './user-category.service';

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [UserCategory]
    ),
  ],
  providers: [UserCategoryService]
})
export class UserCategoryModule { }
