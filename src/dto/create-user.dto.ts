import { IsArray, isArray, IsString, IsNotEmpty } from 'class-validator';
import { UserCategory } from 'src/models/user-category.entity';

export class CreateUserDto {
  readonly id: string;
  @IsString()
  @IsNotEmpty()
  readonly firstName: string;
  @IsString()
  @IsNotEmpty()
  readonly lastName: string;
  @IsString()
  @IsNotEmpty()
  readonly imageUrl: string;
  @IsString()
  @IsNotEmpty()
  readonly email: string;
  @IsArray()
  @IsNotEmpty()
  userCategory: UserCategory[]
}
