import { IsString } from 'class-validator';

export class AccessTokenDto {
    constructor(access_token_id: string, refresh_token_id: string) {
        this.access_token_id = access_token_id;
        this.refresh_token_id = refresh_token_id;
    }
    @IsString()
    readonly access_token_id: string;
    @IsString()
    readonly refresh_token_id: string;

}
