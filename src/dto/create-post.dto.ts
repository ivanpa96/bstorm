import { IsNumber, IsString } from 'class-validator';
import { User } from 'src/models/user.entity';

export class CreatePostDto {
  @IsString()
  readonly title: string;
  @IsString()
  readonly description: string;
  @IsString()
  readonly image: string;
  @IsString()
  user: User;
  @IsNumber()
  likes: number;

}
