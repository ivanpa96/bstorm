import { IsString } from 'class-validator';

export class UpdateUserCategoryDto {
  @IsString()
  readonly categoryId: string;

}
