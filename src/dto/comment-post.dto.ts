import { IsString, IsNotEmpty } from 'class-validator';

export class CommentPostDto {
  @IsString()
  @IsNotEmpty()
  readonly text: string;

}
