import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailModule } from 'src/mail/mail.module';
import { MailService } from 'src/mail/mail.service';
import { AccessToken } from 'src/models/access-token.entity';
import { RefreshToken } from 'src/models/refresh-token.entity';
import { UserCategory } from 'src/models/user-category.entity';
import { User } from 'src/models/user.entity';
import { UserCategoryModule } from 'src/user-category/user-category.module';
import { UserCategoryService } from 'src/user-category/user-category.service';
import { UserModule } from 'src/user/user.module';
import { UserService } from 'src/user/user.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategies/jwt.strategies';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User, AccessToken, RefreshToken, UserCategory
    ]),
    JwtModule.register({
      secret: '<SECRET_KEY>',
      signOptions: {
        expiresIn: '5m'
      }
    }),
    UserModule,
    MailModule,
    UserCategoryModule
  ],
  controllers: [AuthController],
  providers: [AuthService, UserService, JwtStrategy, MailService, UserCategoryService],
  exports: [JwtStrategy]
})
export class AuthModule { }
