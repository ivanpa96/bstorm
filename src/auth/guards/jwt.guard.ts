import { Injectable, UnauthorizedException, CanActivate, ExecutionContext, NotAcceptableException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";


@Injectable()
export class JWTGuard extends AuthGuard('jwt') {

    canActivate(context: ExecutionContext) {
        return super.canActivate(context);
    }

    handleRequest(err, user, info: Error) {
        if (err || info || !user) {
            throw new NotAcceptableException('Access token invalid') || info || new UnauthorizedException()
        }

        return user
    }
}
