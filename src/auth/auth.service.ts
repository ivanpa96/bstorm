import { Injectable, NotFoundException } from '@nestjs/common';
import { User } from '../models/user.entity';
import { AccessToken } from '../models/access-token.entity';
import { RefreshToken } from '../models/refresh-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Inject } from '@nestjs/common';
import { UserService } from 'src/user/user.service';
import { LoginRequest, RefreshRequest } from 'src/common/request';
import { SignOptions, TokenExpiredError } from 'jsonwebtoken'
import { JwtService } from '@nestjs/jwt'
import { UnprocessableEntityException } from '@nestjs/common';
import { UnauthorizedException } from '@nestjs/common';
import { MailService } from 'src/mail/mail.service';

const BASE_OPTIONS: SignOptions = {
    issuer: 'https://my-app.com',
    audience: 'https://my-app.com',
}

export interface RefreshTokenPayload {
    jti: number;
    sub: number
}

export interface AccessTokenPayload {
    jti: number;
    sub: number
}

export interface AuthenticationPayload {
    user: User
    payload: {
        type: string
        access_token?: string
        refresh_token?: string
    }
}

@Injectable()
export class AuthService {
    @InjectRepository(AccessToken)
    accessTokenRepository: Repository<AccessToken>;
    @InjectRepository(User)
    userRepository: Repository<User>;
    @InjectRepository(RefreshToken)
    refreshTokenRepository: Repository<RefreshToken>;
    @Inject()
    userService: UserService;
    @Inject()
    jwtService: JwtService;
    @Inject()
    mailService: MailService;


    /**
     * Login user
     * @param loginRequest 
     */
    async login(loginRequest: LoginRequest) {
        const { email } = loginRequest;

        const user = await this.userService.checkEmail(email);
        if (!user)
            throw new NotFoundException('User not found')

        const { access_token_object, access_token } = await this.generateAccessToken(user, 60 * 60 * 24 * 30);
        const refresh_token = await this.generateRefreshToken(user, access_token_object, 60 * 60 * 24 * 30);

        const payload = this.buildResponsePayload(user, access_token, refresh_token);
        await this.mailService.sendLoginMail(payload.user, access_token, refresh_token);
        return {
            status: 'success',
            // data: payload
            message: "Email sent successfully!"
        }
    }

    /**
     * Refresh access_token
     * @param refreshRequest 
     * @returns New access and refresh token
     */
    async refreshToken(refreshRequest: RefreshRequest) {

        const { user, access_token, new_refresh_token } = await this.createAccessTokenFromRefreshToken(refreshRequest.refresh_token)

        const payload = this.buildResponsePayload(user, access_token, new_refresh_token);

        return {
            status: 'success',
            data: payload
        }
    }

    /**
     * Save access token
     * @param user 
     * @param ttl 
     * @returns Saved access token
     */
    async saveAccessToken(user: User, ttl: number): Promise<AccessToken> {

        const access_token = new AccessToken();
        // access_token.user_id = user.id;
        access_token.user_id = user;
        access_token.revoked = false;
        const expiration = new Date();
        expiration.setTime(expiration.getTime() + ttl);

        access_token.expires_at = expiration;

        return await this.accessTokenRepository.save(access_token);
    }

    /**
     * Generate access token
     * @param user 
     * @param ttl 
     * @returns Generated access_token
     */
    async generateAccessToken(user: User, ttl: number): Promise<{ access_token_object: AccessToken, access_token: string }> {

        const access_token_object = await this.saveAccessToken(user, ttl)
        const opts: SignOptions = {
            ...BASE_OPTIONS,
            subject: String(user.id),
            jwtid: access_token_object.id
        }

        const access_token = (await this.jwtService.signAsync({}, opts)).toString();

        return {
            access_token_object,
            access_token
        };
    }

    /**
     * Save refresh token
     * @param user 
     * @param access_token 
     * @param ttl 
     * @returns Saved refresh token
     */
    async saveRefreshToken(user: User, access_token: AccessToken, ttl: number): Promise<RefreshToken> {
        const refresh_token = new RefreshToken();
        refresh_token.access_token_id = access_token.id;
        refresh_token.revoked = false;
        const expiration = new Date();
        expiration.setTime(expiration.getTime() + ttl);
        refresh_token.expires_at = expiration;

        return await this.refreshTokenRepository.save(refresh_token);
    }

    /**
     * Generate refresh token
     * @param user 
     * @param access_token 
     * @param ttl 
     * @returns Generated refresh token
     */
    async generateRefreshToken(user: User, access_token: AccessToken, ttl: number): Promise<string> {

        const refresh_token = await this.saveRefreshToken(user, access_token, ttl);

        const opts: SignOptions = {
            ...BASE_OPTIONS,
            expiresIn: ttl,
            subject: String(access_token.id),
            jwtid: String(refresh_token.id)
        }

        return this.jwtService.signAsync({}, opts);
    }

    /**
     * Generate new access token from refresh token
     * @param refresh 
     * @returns 
     */
    async createAccessTokenFromRefreshToken(refresh: string): Promise<{ access_token: string, user: User, new_refresh_token: string }> {

        const { user, refresh_token, payload } = await this.resolveRefreshToken(refresh);
        const updated_refresh_token = await this.updateRefreshToken(payload.jti);
        const updated_access_token = await this.updateAccessToken(payload.sub);

        const { access_token_object, access_token } = await this.generateAccessToken(user, 60 * 60 * 24 * 30);
        const new_refresh_token = await this.generateRefreshToken(user, access_token_object, 60 * 60 * 24 * 30);

        return { user, access_token, new_refresh_token }
    }

    /**
     * Resolve refresh token
     * @param encoded 
     * @returns User, refresh token, refresh token payload
     */
    async resolveRefreshToken(encoded: string): Promise<{ user: User, refresh_token: RefreshToken, payload: RefreshTokenPayload }> {
        const payload = await this.decodeRefreshToken(encoded);
        const refresh_token = await this.getStoredTokenFromRefreshTokenPayload(payload);

        if (!refresh_token) {
            throw new UnprocessableEntityException('Refresh token not found')
        }

        if (refresh_token.revoked) {
            throw new UnprocessableEntityException('Refresh token revoked')
        }

        const user = await this.getUserFromRefreshTokenPayload(payload)

        if (!user) {
            throw new UnprocessableEntityException('Refresh token malformed')
        }

        return { user, refresh_token, payload }
    }

    /**
    * Decode refresh token
    * @param token 
    * @returns Refresh token payload
    */
    async decodeRefreshToken(token: string): Promise<RefreshTokenPayload> {
        try {
            return this.jwtService.verifyAsync(token)
        } catch (e) {
            if (e instanceof TokenExpiredError) {
                throw new UnprocessableEntityException('Refresh token expired')
            } else {
                throw new UnprocessableEntityException('Refresh token malformed')
            }
        }
    }

    /**
     * Decode access token
     * @param token 
     * @returns Access token payload
     */
    async decodeAccessToken(token: string): Promise<AccessTokenPayload> {
        try {
            return this.jwtService.verifyAsync(token)
        } catch (e) {
            if (e instanceof TokenExpiredError) {
                throw new UnprocessableEntityException('Access token expired')
            } else {
                throw new UnprocessableEntityException('Access token malformed')
            }
        }
    }

    /**
     * Get refresh token from payload
     * @param payload 
     * @returns Refresh token or null
     */
    async getStoredTokenFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<RefreshToken | null> {
        const tokenId = payload.jti;
        if (!tokenId) {
            throw new UnprocessableEntityException('Refresh token malformed');
        }
        return this.findRefreshTokenById(tokenId);
    }


    /**
     * Get user from refresh token
     * @param payload 
     * @returns User
     */
    async getUserFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<User> {
        const subId = payload.sub;

        console.log("REFRESH_TOKEN_ID")
        console.log(payload.jti)

        if (!subId) {
            throw new UnprocessableEntityException('Refresh token malformed')
        }
        const access_token = await this.findAccessTokenById(subId);
        console.log("REFRESH_TOKEN")
        console.log(access_token.user_id)

        return this.userService.findOne(access_token.user_id.id);
    }

    /**
     * Get user from access token
     * @param payload 
     * @returns User
     */
    async getUserFromAccessTokenPayload(payload: AccessTokenPayload): Promise<User> {
        const userId = payload.sub;

        if (!userId) {
            throw new UnprocessableEntityException('Access token malformed')
        }

        const access_token = await this.findAccessTokenById(payload.jti);

        return this.userService.findOne(userId.toString());
    }

    /**
     * Find refresh token by id
     * @param id 
     * @returns Refresh token or null
     */
    async findRefreshTokenById(id: number): Promise<RefreshToken | null> {
        return this.refreshTokenRepository.findOne({
            where: {
                id
            }
        })
    }

    /**
     * Find access token by id
     * @param id 
     * @returns Access token or null
     */
    async findAccessTokenById(id: number): Promise<AccessToken | null> {

        return await this.accessTokenRepository.findOne({
            where: {
                id
            },
            relations: ['user_id']
        })

    }

    /**
     * Find refresh token by access token
     * @param id 
     * @returns Refresh token or null
     */
    async findRefreshTokenByAccessTokenId(id: number): Promise<RefreshToken | null> {
        return this.refreshTokenRepository.findOne({
            where: {
                access_token_id: id
            }
        })
    }

    /**
     * Build response
     * @param user 
     * @param access_token 
     * @param refresh_token 
     * @returns Payload
     */
    buildResponsePayload(user: User, access_token?: string, refresh_token?: string): AuthenticationPayload {
        return {
            user: user,
            payload: {
                type: 'Bearer',
                ...(access_token ? { access_token: access_token } : {}),
                ...(refresh_token ? { refresh_token: refresh_token } : {}),
            }
        }
    }

    /**
     * Update access token
     * @param id 
     * @returns Access token
     */
    async updateAccessToken(id: number): Promise<AccessToken> {
        const access_token = await this.accessTokenRepository.findOne({
            where: {
                id
            },

        })
        access_token.revoked = true;
        const updateTime = new Date()
        updateTime.setTime(updateTime.getTime())
        access_token.updated_at = updateTime;

        return this.accessTokenRepository.save(access_token);
    }

    /**
     * Update refresh token
     * @param id 
     * @returns Refresh token
     */
    async updateRefreshToken(id: number): Promise<RefreshToken> {
        const refresh_token = await this.refreshTokenRepository.findOne({
            where: {
                id
            },
        })
        refresh_token.revoked = true;
        const updateTime = new Date()
        updateTime.setTime(updateTime.getTime())
        refresh_token.updated_at = updateTime;

        return this.refreshTokenRepository.save(refresh_token);
    }

    /**
     * Resolve access token
     * @param payload 
     * @returns User and access token
     */
    async resolveAccessToken(payload: AccessTokenPayload): Promise<{ user: User, token: AccessToken }> {
        // const payload = await this.decodeAccessToken(encoded)


        const token = await this.findAccessTokenById(payload.jti)
        if (!token) {
            throw new UnprocessableEntityException('Access token not found')
        }

        if (token.revoked) {
            throw new UnprocessableEntityException('Access token revoked')
        }

        const user = await this.getUserFromAccessTokenPayload(payload)

        if (!user) {
            throw new UnprocessableEntityException('Access token malformed')
        }

        return { user, token }
    }

    /**
     * Auth user
     * @param access_token 
     * @returns User, access and refresh token
     */
    async authenticateUser(access_token: string) {
        const payload = await this.decodeAccessToken(access_token);

        const { user, token } = await this.resolveAccessToken(payload)
        const refresh_token = await this.findRefreshTokenByAccessTokenId(payload.jti)
        if (!user) {
            return new UnauthorizedException();
        }

        const opts: SignOptions = {
            ...BASE_OPTIONS,
            expiresIn: 60 * 60 * 24 * 30,
            subject: String(token.id),
            jwtid: String(refresh_token.id)
        }

        const rt = await this.jwtService.signAsync({}, opts);
        // const p = this.buildResponsePayload(user, access_token, rt);
        return {
            status: 'success',
            user: user,
            access_token: access_token,
            refresh_token: rt
        }
    }

}
