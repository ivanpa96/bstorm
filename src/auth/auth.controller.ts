import {
    Controller,
    Get,
    Post,
    Body,
    Param,
    Delete,
    Patch,
    Inject,
    Query,
    UseInterceptors,
    ClassSerializerInterceptor,
    Res,
    UseGuards,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { LoginRequest, RefreshRequest } from 'src/common/request';
import { Req } from '@nestjs/common';
import { User } from 'src/models/user.entity';
import { UnauthorizedException } from '@nestjs/common';
import { Exception } from 'handlebars';
import { Response } from 'express';
import { ThrottlerGuard } from '@nestjs/throttler';

@Controller('/api/auth')
export class AuthController {
    @Inject()
    userService: UserService;
    @Inject()
    authService: AuthService;


    /**
     * Login User
     * @param body 
     * @returns 
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @UseGuards(ThrottlerGuard)
    @Post("/login")
    create(@Body() body: LoginRequest) {
        return this.authService.login(body);
    }

    /**
    * Refrsh Access Token
    * @param body 
    * @returns 
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Post("/refresh")
    refresh(@Body() body: RefreshRequest) {
        return this.authService.refreshToken(body);
    }

    /**
     * Authenticate user from magic link
     * @param request 
     * @returns User
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @UseGuards(ThrottlerGuard)
    @Get()
    async authenticate(@Req() request) {
        return await this.authService.authenticateUser(request.query.token);
    }
}
