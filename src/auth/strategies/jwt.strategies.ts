import { Injectable, UnauthorizedException, UnprocessableEntityException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from '../../user/user.service';
import { User } from '../../models/user.entity';;
import { Inject } from '@nestjs/common';
import { AuthService } from '../auth.service';

export interface AccessTokenPayload {
    sub: number;
    jti: number;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    @Inject()
    userService: UserService;
    @Inject()
    private authService: AuthService;

    public constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: '<SECRET_KEY>',
            signOptions: {
                expiresIn: '5m',
            },
        })
        // this.authService = authService;

    }

    async validate(payload: AccessTokenPayload): Promise<User | any> {
        const { user, token } = await this.authService.resolveAccessToken(payload)

        if (!user) {
            return new UnauthorizedException();
        }

        return user
    }
}
