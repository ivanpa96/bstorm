import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, Timestamp } from 'typeorm';
import { uuid } from 'uuidv4';
import { User } from './user.entity'
import { RefreshToken } from './refresh-token.entity';

@Entity()
export class AccessToken {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: false })
    revoked: boolean

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    expires_at: Date;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @ManyToOne(() => User, user => user.id, {
        onDelete: 'CASCADE'
    })
    @JoinColumn({ name: 'user_id' })
    user_id: User

    @OneToMany(() => RefreshToken, refreshToken => refreshToken.access_token_id)
    refresh_token_id: RefreshToken
}



