import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { AccessToken } from 'src/models/access-token.entity';
import { UserCategory } from './user-category.entity';
import { Post } from './post.entity';
import { Like } from './like.entity';
import { Comment } from './comment.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({ nullable: false })
    firstName: string;
    @Column({ nullable: false })
    lastName: string;
    @Column({ nullable: false })
    imageUrl: string;
    @Column({ nullable: false })
    email: string;
    @Column({ nullable: true, default: 0 })
    points: number = 0;

    @OneToMany(() => AccessToken, accessToken => accessToken.user_id)
    access_token: AccessToken

    @OneToMany((type) => UserCategory, uc => uc.user, {
        persistence: false,
        onDelete: 'CASCADE',
        eager: true
    })
    userCategory?: UserCategory[];

    @OneToMany(() => Post, post => post.user)
    post: Post

    @OneToMany(() => Like, like => like.user_liked)
    like: Like

    @OneToMany(() => Comment, comment => comment.user_commented)
    comment: Comment

}
