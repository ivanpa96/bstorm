import { Post } from 'src/models/post.entity';
import { User } from 'src/models/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Like {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  count: number = 0;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  updated_at: Date;

  @ManyToOne(() => User, user => user, { eager: true })
  @JoinColumn({ name: 'user_liked_id' })
  user_liked: User

  @ManyToOne(() => Post, post => post.id)
  @JoinColumn({ name: 'post_id' })
  post: Post


}
