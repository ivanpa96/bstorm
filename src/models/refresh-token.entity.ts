import { Column, Entity, JoinColumn, OneToOne, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn, Timestamp } from 'typeorm';
import { User } from './user.entity'
import { AccessToken } from './access-token.entity';

@Entity()
export class RefreshToken {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: false })
    revoked: boolean

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    expires_at: Date;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    updated_at: Date;

    @ManyToOne(() => AccessToken, accessToken => accessToken.refresh_token_id, {
        eager: true,
        onDelete: 'CASCADE'
    })
    @JoinColumn({ name: 'access_token_id' })
    access_token_id: string


}



