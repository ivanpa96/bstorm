import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, Timestamp } from 'typeorm';
import { User } from './user.entity'
import { RefreshToken } from './refresh-token.entity';
import { Category } from './category.entity';

@Entity()
export class UserCategory {

    constructor(user_id: string, category_id: string) {
        this.userId = user_id;
        this.categoryId = category_id;
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    categoryId: string

    @Column()
    userId: string

    @ManyToOne(() => User, user => user.userCategory, {
        primary: true,
        persistence: false,
        onDelete: 'CASCADE'
    })
    user: User

    @ManyToOne(() => Category, category => category.userCategory, {
        primary: true,
        persistence: false
    })
    category: Category

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    created_at: Date;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    updated_at: Date;

}



