import { User } from 'src/models/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category.entity';
import { Comment } from './comment.entity';
import { Like } from './like.entity';

@Entity()
export class Post {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  image: string;

  @Column()
  total_likes: number = 0;

  @Column()
  total_comments: number = 0;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  created_at: Date;

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  updated_at: Date;

  @ManyToOne(() => User, user => user.id, { eager: true })
  @JoinColumn({ name: 'user_id' })
  user: User

  @OneToMany(() => Like, like => like.post, { eager: true, onDelete: 'CASCADE', persistence: false })
  like: Like

  @OneToMany(() => Comment, comment => comment.post, { eager: true, onDelete: 'CASCADE', persistence: false })
  comment: Comment

  @ManyToOne(() => Category, category => category.id, { eager: true })
  @JoinColumn({ name: 'category_id' })
  category: Category


}
