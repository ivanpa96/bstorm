import { Inject, Injectable, InternalServerErrorException, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { throws } from 'assert';
import { plainToClass } from 'class-transformer';
import { CommentService } from 'src/comment/comment.service';
import { CommentPostDto } from 'src/dto/comment-post.dto';
import { UpdatePostDto } from 'src/dto/update-post.dto';
import { LikeService } from 'src/like/like.service';
import { Comment } from 'src/models/comment.entity';
import { Like } from 'src/models/like.entity';
import { User } from 'src/models/user.entity';
import { UserService } from 'src/user/user.service';
import { EntityManager, Repository, Transaction, TransactionRepository, Connection, getManager, getConnection, In } from 'typeorm';
import { CreatePostDto } from '../dto/create-post.dto';
import { Post } from '../models/post.entity';


@Injectable()
export class PostService {
    @InjectRepository(Post)
    postRepository: Repository<Post>;
    @InjectRepository(Like)
    likeRepository: Repository<Like>;
    @Inject()
    usersService: UserService;
    @Inject()
    likeService: LikeService;
    @Inject()
    commentService: CommentService;
    @Inject()
    connection: Connection

    /**
     * Save new post
     * @param createPostDto 
     * @param user_id 
     * @returns Post
     */
    async create(createPostDto: CreatePostDto, user_id: string): Promise<Post> {
        const user = await this.usersService.findOne(user_id);
        if (!user) {
            throw new NotFoundException("User not found!");
        }
        createPostDto.user = user;
        const post = this.postRepository.create(createPostDto);

        return this.postRepository.save(post);
    }

    /**
     * Update post
     * @param id 
     * @param updatePostDto 
     * @returns Post
     */
    async update(id: string, updatePostDto: UpdatePostDto): Promise<Post> {
        const post = await this.postRepository.preload({
            id: id,
            ...updatePostDto,
        });
        if (!post) {
            throw new NotFoundException(`Post with ID=${id} not found`);
        }
        return this.postRepository.save(post);
    }

    /**
     * Find post by Id
     * @param id 
     * @returns Post
     */
    async findOne(id: string): Promise<Post> {
        const post = await this.postRepository.findOne({
            where: {
                id
            },
        });
        if (!post) {
            throw new NotFoundException('Post not found');
        }
        return post;
    }

    /**
     * Get all posts
     * @returns Array of posts
     */
    async findAll(): Promise<Post[]> {
        const posts = await this.postRepository.find();

        return posts;
    }

    /**
     * Sort post by creation date ASC
     * @returns Array of posts sorted by creation date asc
     */
    async sortByCreationDateASC(): Promise<Post[]> {
        return await this.postRepository.find({ order: { created_at: 'ASC' } });
    }

    /**
     * Sort post by creation date DESC
     * @returns Array of posts sorted by creation date desc
     */
    async sortByCreationDateDESC(): Promise<Post[]> {
        return await this.postRepository.find({ order: { created_at: 'DESC' } });
    }

    /**
     * Sort post by popularity ASC
     * @returns Array of posts sorted by popularity asc
     */
    async sortByPopularityASC(): Promise<Post[]> {
        return await this.postRepository.find({ order: { total_likes: 'ASC', total_comments: 'ASC' } });
    }

    /**
     * Sort post by popularity DESC
     * @returns Array of posts sorted by popularity desc
     */
    async sortByPopularityDESC(): Promise<Post[]> {
        return await this.postRepository.find({ order: { total_likes: 'DESC', total_comments: 'DESC' } });
    }

    /**
     * Filter post by category
     * @param id 
     * @returns Array of posts filtered by category
     */
    async filterByCategory(id: string): Promise<Post[]> {
        return await this.postRepository.find({
            where: {
                category: id
            }
        });
    }

    /**
     * Filter post by creator's name
     * @param alias 
     * @param firstName 
     * @param lastName 
     * @returns Arayy of posts filtered by post's creator name
     */
    async searchByFirstLastName(alias: string, firstName?: string, lastName?: string): Promise<Post[]> {
        const queryBuilder = this.postRepository.createQueryBuilder(alias)
            .leftJoinAndSelect(`${alias}.user`, 'user');

        if (firstName && lastName) {
            queryBuilder.where("user.firstName LIKE :firstName and user.lastName LIKE :lastName",
                { firstName: `%${firstName}%`, lastName: `%${lastName}%` }
            );
        } else {
            if (firstName) {
                queryBuilder.where("user.firstName LIKE :s ", { s: `%${firstName}%` });
            }
            if (lastName) {
                queryBuilder.where("user.lastName LIKE :s ", { s: `%${lastName}%` });
            }
        }
        return await queryBuilder.getMany();
    }

    /**
     * Get posts for specific user (based on user's preferences)
     * @param user 
     * @returns Array of posts based on user preferences
     */
    async getAllPostsByUserCategory(user: User): Promise<Post[]> {

        var categories: string[] = [];
        user.userCategory.forEach(element => {
            categories.push(element.categoryId)
        });
        return await this.postRepository.find({
            where: {
                category: { id: In(categories) }
            },
            order: { total_likes: 'DESC', total_comments: 'DESC' }
        })
    }

    /**
     * Delete post
     * @param id 
     * @returns Delete post
     */
    async remove(id: string): Promise<Post> {
        const post = await this.findOne(id);
        return this.postRepository.remove(post);
    }

    /**
     * Like a post
     * @param post_id 
     * @param user 
     * @returns Like
     */
    async likePost(post_id: string, user: User): Promise<Like> {
        var like = null;
        const post = await this.findOne(post_id);

        if (post.user.id === user.id) {
            throw new UnprocessableEntityException(`Cannot like your own post!`);
        }

        const oldLike = await this.likeService.findLike(post, user);
        if (!oldLike) {
            const newLike = new Like();
            newLike.count = ++newLike.count;
            newLike.user_liked = user;
            newLike.post = post;
            like = newLike;
        } else {
            if (oldLike.user_liked.id == user.id) {
                throw new UnprocessableEntityException('Cannot like post twice')
            }
            oldLike.count = ++oldLike.count;
            like = oldLike;
        }

        const queryRunner = this.connection.createQueryRunner();
        if (post) {
            await queryRunner.connect();

            try {
                await queryRunner.startTransaction();
                await queryRunner.manager.insert('like', like);
                post.total_likes = ++post.total_likes;
                await queryRunner.manager.save(post);

                post.user.points = ++post.user.points;
                await queryRunner.manager.save(post.user);

                await queryRunner.commitTransaction();
            } catch (error) {
                await queryRunner.rollbackTransaction()
                throw new InternalServerErrorException();
            } finally {
                await queryRunner.release();
            }
        }

        return like;
    }

    /**
     * Comment a post
     * @param post_id 
     * @param user 
     * @param body 
     * @returns Comment
     */
    async commentPost(post_id: string, user: User, body: string): Promise<Comment> {
        var jsonObject = body as Object;
        var commentPostDto = plainToClass(CommentPostDto, jsonObject)
        const post = await this.findOne(post_id);

        const newComment = new Comment();
        newComment.text = commentPostDto.text;
        newComment.user_commented = user;
        newComment.post = post;

        const queryRunner = this.connection.createQueryRunner();
        if (post) {
            await queryRunner.connect();

            try {
                await queryRunner.startTransaction();
                await queryRunner.manager.insert('comment', newComment);
                post.total_comments = ++post.total_comments;

                await queryRunner.manager.save(post);

                await queryRunner.commitTransaction();
            } catch (error) {
                await queryRunner.rollbackTransaction()
                throw new InternalServerErrorException();
            } finally {
                await queryRunner.release();
            }
        }

        return newComment;
    }

    /**
     * Update post's total likes
     * @param post 
     * @returns Post
     */
    async updateTotalLikes(post: Post): Promise<Post> {
        post.total_likes = ++post.total_likes;

        return await this.postRepository.save(post);
    }

    /**
     * Update post's total comments
     * @param post 
     * @returns Post
     */
    async updateTotalComments(post: Post): Promise<Post> {
        post.total_comments = ++post.total_comments;

        return await this.postRepository.save(post);
    }
}
