import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentModule } from 'src/comment/comment.module';
import { CommentService } from 'src/comment/comment.service';
import { LikeModule } from 'src/like/like.module';
import { LikeService } from 'src/like/like.service';
import { Comment } from 'src/models/comment.entity';
import { Like } from 'src/models/like.entity';
import { Post } from 'src/models/post.entity';
import { UserCategory } from 'src/models/user-category.entity';
import { User } from 'src/models/user.entity';
import { UserCategoryService } from 'src/user-category/user-category.service';
import { UserModule } from 'src/user/user.module';
import { UserService } from 'src/user/user.service';
import { PostController } from './post.controller';
import { PostService } from './post.service';

@Module({
  imports: [TypeOrmModule.forFeature([
    Post, User, Like, UserCategory, Comment
  ]),
    UserModule,
    LikeModule,
    CommentModule
  ],
  controllers: [PostController],
  providers: [PostService, UserService, UserCategoryService, LikeService, CommentService]
})
export class PostModule { }
