import { UploadedFile, Request, ClassSerializerInterceptor, Controller, Delete, Get, Patch, Inject, Param, Post, Req, UseGuards, UseInterceptors, Body } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express/multer';
import { JWTGuard } from 'src/auth/guards/jwt.guard';
import { Like } from 'src/models/like.entity';
import { Post as PostEntity } from '../models/post.entity';
import { PostService } from './post.service';
import { storage } from 'src/common/storage';
import { UpdatePostDto } from 'src/dto/update-post.dto';
import { Comment } from 'src/models/comment.entity';

@Controller('/api/post')
export class PostController {
    @Inject()
    postService: PostService;


    /**
    * Create new Post
    * @param request 
    * @returns Post
    */
    @UseInterceptors(ClassSerializerInterceptor,)
    @Post()
    @UseGuards(JWTGuard)
    async savePost(@Req() request): Promise<PostEntity> {
        const access_token = request.headers.authorization.toString().split(" ")[1];
        const user_id = request.user.id

        return await this.postService.create(request.body, user_id);
    }

    /**
     * Upload post file 
     * @param id 
     * @param file 
     * @returns file_path
     */
    @Post('/file')
    @UseGuards(JWTGuard)
    @UseInterceptors(FileInterceptor('file', storage))
    async updateFile(@UploadedFile() file) {
        return file.path;
    }

    /**
    * Get Post by Id
    * @param id 
    * @returns Post
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get(':id')
    @UseGuards(JWTGuard)
    getPostById(@Param('id') id: string): Promise<PostEntity> {
        return this.postService.findOne(id);
    }

    /**
    * Get all Posts
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get()
    @UseGuards(JWTGuard)
    getAllPosts(): Promise<PostEntity[]> {
        return this.postService.findAll();
    }

    /**
    * Sort Posts by creation date ASC
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/sort/date/asc')
    @UseGuards(JWTGuard)
    sortByCreationDateASC(): Promise<PostEntity[]> {
        return this.postService.sortByCreationDateASC();
    }

    /**
    * Sort Posts by creation date DESC
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/sort/date/desc')
    @UseGuards(JWTGuard)
    sortByCreationDateDESC(): Promise<PostEntity[]> {
        return this.postService.sortByCreationDateDESC();
    }

    /**
    * Sort Posts by popularity ASC
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/sort/popularity/asc')
    @UseGuards(JWTGuard)
    sortByPopularityASC(): Promise<PostEntity[]> {
        return this.postService.sortByPopularityASC();
    }

    /**
    * Sort Posts by popularity DESC
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/sort/popularity/desc')
    @UseGuards(JWTGuard)
    sortByPopularityDESC(): Promise<PostEntity[]> {
        return this.postService.sortByPopularityDESC();
    }


    /**
    * Filter Posts by category
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/filter/:id')
    @UseGuards(JWTGuard)
    filterByCategory(@Param('id') id: string): Promise<PostEntity[]> {
        return this.postService.filterByCategory(id);
    }

    /**
     * Search Posts by User first/last name
     * @param req 
     * @returns Posts
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/find/search')
    @UseGuards(JWTGuard)
    async searchPosts(@Req() req) {
        return await this.postService.searchByFirstLastName('post', req.query.firstName, req.query.lastName);
    }

    /**
    * Get Posts by logged user categories
    * @param req 
    * @returns Posts
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/find/all')
    @UseGuards(JWTGuard)
    async getAllPostsByUserCategory(@Req() req) {
        return await this.postService.getAllPostsByUserCategory(req.user);
    }

    /**
     * Update Post
     * @param id 
     * @param updateUserDto 
     * @returns Post
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Patch(':id')
    @UseGuards(JWTGuard)
    update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto): Promise<PostEntity> {
        return this.postService.update(id, updatePostDto);
    }

    /**
     * Delete Post
     * @param id 
     * @returns 
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Delete(':id')
    @UseGuards(JWTGuard)
    remove(@Param('id') id: string): Promise<PostEntity> {
        return this.postService.remove(id);
    }

    /**
     * Like a Post
     * @param id 
     * @param request 
     * @returns Post
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get("/like/:id")
    @UseGuards(JWTGuard)
    async likePost(@Param('id') id: string, @Req() request): Promise<Like> {
        return await this.postService.likePost(id, request.user);
    }


    /**
     * Comment a Post
     * @param id 
     * @param request 
     * @returns Post
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Post("/comment/:id")
    @UseGuards(JWTGuard)
    async commentPost(@Param('id') id: string, @Req() request): Promise<Comment> {

        return await this.postService.commentPost(id, request.user, request.body);
    }


}
