import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config, throttleConfig } from '../ormconfig'
import { MailModule } from './mail/mail.module';
import { UserCategoryModule } from './user-category/user-category.module';
import { PostModule } from './post/post.module';
import { LikeModule } from './like/like.module';
import { CommentModule } from './comment/comment.module';
import { ThrottlerModule } from '@nestjs/throttler';
import { TranslatorModule } from 'nestjs-translator';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    // TypeOrmModule.forRoot(config),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      autoLoadEntities: true,
      synchronize: true,
      entities: ['dist/**/*.entity.js'],
      migrations: ['src/migration/*.js'],
      cli: {
        migrationsDir: 'src/migration'
      },
    }),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 5
    }),
    TranslatorModule.forRoot({
      global: true,
      defaultLang: 'en',
      translationSource: '/src/i18n',
      requestKeyExtractor: req => req.get('language')
    }),
    UserModule,
    AuthModule,
    MailModule,
    UserCategoryModule,
    PostModule,
    LikeModule,
    CommentModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
