import { Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../models/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { Inject } from '@nestjs/common';
import { UserCategoryService } from 'src/user-category/user-category.service';
import { UserCategory } from 'src/models/user-category.entity';
import { TranslatorService } from 'nestjs-translator';
import { request } from 'express';
import { UpdateUserCategoryDto } from 'src/dto/update-user-category.dto';
import { InternalServerErrorException } from '@nestjs/common';

@Injectable()
export class UserService {
    @InjectRepository(User)
    userRepository: Repository<User>;
    @Inject()
    userCategoryService: UserCategoryService;
    @Inject()
    connection: Connection;
    @Inject()
    translatorService: TranslatorService


    /**
     * Get all users from database
     * @param limit 
     * @param offset 
     * @returns Array of Users
     */
    findAll(limit = 10, offset = 0): Promise<User[]> {
        return this.userRepository.find({
            skip: offset,
            take: limit,
        });
    }

    /**
     * Find User by Id
     * @param id 
     * @param language 
     * @returns User
     */
    async findOne(id: string, language?: string): Promise<User> {
        const user = await this.userRepository.findOne({
            where: {
                id
            },
        });
        if (!user) {
            throw new NotFoundException(`${this.translatorService.translate('USER_NOT_FOUND', {
                lang: language
            })}`);
        }
        return user;
    }

    /**
     * Update User
     * @param id 
     * @param updateUserDto 
     * @returns Updated User
     */
    async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
        const user = await this.userRepository.preload({
            id: id,
            ...updateUserDto,
        });
        if (!user) {
            throw new NotFoundException(`User with ID=${id} not found`);
        }
        return this.userRepository.save(user);
    }

    /**
     * Delete User from database
     * @param id 
     * @returns Removed User
     */
    async remove(id: string): Promise<User> {
        const user = await this.findOne(id);

        return this.userRepository.remove(user);
    }

    /**
     * Check if email already exists
     * @param email 
     * @returns User or null
     */
    async checkEmail(email: String): Promise<User | null> {
        return this.userRepository.findOne({ where: { email } });
    }

    /**
     * Update user's points
     * @param user 
     * @returns User
     */
    async updatePoints(user: User) {
        console.log(user.points)
        user.points = ++user.points;

        return await this.userRepository.save(user);
    }


    /**
     * Create new User
     * @param createUserDto 
     * @param language 
     * @returns User
     */
    async create(createUserDto: CreateUserDto, language?: string): Promise<User> {

        var userCategory: UserCategory[] = [];
        var user = null;
        const email = createUserDto.email;
        const existingEmail = await this.checkEmail(email);
        if (existingEmail) {
            throw new UnprocessableEntityException(`${this.translatorService.translate('EMAIL_IN_USE', {
                lang: language
            })}`);
        }

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        try {

            await queryRunner.startTransaction();
            user = await this.userRepository.save(createUserDto);
            createUserDto.userCategory.forEach(element => {
                const uc = new UserCategory(user.id, element.categoryId.toString());
                userCategory.push(uc);
            });

            if (userCategory.length < 3) {
                throw new UnprocessableEntityException(`${this.translatorService.translate('CATEGORY_MIN', {
                    lang: language
                })}`);
            }

            for (let i = 0; i < userCategory.length - 1; i++) {
                for (let j = i + 1; j < userCategory.length; j++) {
                    if (userCategory[i].categoryId === userCategory[j].categoryId) {
                        throw new UnprocessableEntityException(`${this.translatorService.translate('CATEGORY_DUPLICATE', {
                            lang: language
                        })}`);
                    }
                }
            }
            await queryRunner.manager.save(userCategory);

            await queryRunner.commitTransaction();
        } catch (error) {
            console.log("USAO U CATCH")
            await queryRunner.rollbackTransaction()
            throw new InternalServerErrorException();
        } finally {
            await queryRunner.release();
        }

        return user;
    }

    /**
     * Update User categories
     * @param userCategory 
     * @param user 
     * @returns Array of User categories
     */
    async updateUserCategories(userCategory: UpdateUserCategoryDto[], user: User): Promise<UserCategory[]> {
        return await this.userCategoryService.updateUserCategory(userCategory, user);
    }

    /**
     * Get the most popular User from database
     * @returns Array of Users 
     */
    async getMostPopularUsers(): Promise<User[]> {
        return await this.userRepository.find({
            order: { points: 'DESC' }
        })
    }


}
