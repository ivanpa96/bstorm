import { Post } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from 'src/models/user.entity';
import { UserController } from './user.controller';
import { UserModule } from './user.module';
import { UserService } from './user.service'

describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;

  const mockUserService = {
    create: jest.fn((dto) => {
      return {
        id: Date.now(),
        ...dto
      }
    }),
    update: jest.fn((id, dto) => ({
      id,
      ...dto
    }))
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService]
    }).overrideProvider(UserService).useValue(mockUserService).compile();

    userService = module.get<UserService>(UserService);
    userController = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(userController).toBeDefined();
  })


  it('should update a user', () => {
    const dto = {
      firstName: 'Ivan',
      lastName: 'Pantelic',
      imageUrl: 'C://work',
      email: 'ivan.pa@mts.rs'
    };
    expect(userController.update('1', dto)).toEqual({
      id: 1,
      ...dto
    });
  })

});
