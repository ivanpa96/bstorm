import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccessToken } from 'src/models/access-token.entity';
import { RefreshToken } from 'src/models/refresh-token.entity';
import { UserCategory } from 'src/models/user-category.entity';
import { User } from 'src/models/user.entity';
import { UserCategoryModule } from 'src/user-category/user-category.module';
import { UserCategoryService } from 'src/user-category/user-category.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [User, AccessToken, RefreshToken, UserCategory]
    ),
    UserCategoryModule
  ],
  controllers: [UserController],
  providers: [UserService, UserCategoryService]
})
export class UserModule { }
