import {
    Controller,
    Get,
    Post,
    Body,
    Param,
    Delete,
    Patch,
    Inject,
    UseInterceptors,
    ClassSerializerInterceptor,
    UploadedFile
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../models/user.entity';
import { UseGuards } from '@nestjs/common';
import { JWTGuard } from 'src/auth/guards/jwt.guard';
import { Req } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express/multer';
import { storage } from 'src/common/storage';
import { UpdateUserCategoryDto } from 'src/dto/update-user-category.dto';

@Controller('/api/users')
export class UserController {
    @Inject()
    userService: UserService;

    /**
     * Register User
     * @param createUserDto 
     * @returns User
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Post("/register")
    async create(@Body() createUserDto: CreateUserDto, @Req() req): Promise<User> {
        return await this.userService.create(createUserDto, req.headers.language);
    }


    /**
     * Get User's details
     * @param request 
     * @returns User
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get("/me")
    @UseGuards(JWTGuard)
    async getUser(@Req() request): Promise<User> {
        const access_token = request.headers.authorization.toString().split(" ")[1];
        const userId = request.user.id
        const user = await this.userService.findOne(userId);

        return user;
    }

    /**
     * Get all Users
     * @returns Users
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get()
    @UseGuards(JWTGuard)
    findAll() {
        return this.userService.findAll();
    }

    /**
    * Get the most popular Users
    * @returns Users
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/search/mostpopular')
    @UseGuards(JWTGuard)
    getMostPopularUsers() {
        return this.userService.getMostPopularUsers();
    }

    /**
     * Get User by ID
     * @param id 
     * @returns User
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Get(':id')
    @UseGuards(JWTGuard)
    async findOne(@Param('id') id: string, @Req() req): Promise<User> {
        return await this.userService.findOne(id, req.headers.language);
    }
    /**
     * Update User
     * @param id 
     * @param updateUserDto 
     * @returns User
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Patch(':id')
    @UseGuards(JWTGuard)
    update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto): Promise<User> {
        return this.userService.update(id, updateUserDto);
    }

    /**
     * Delete User
     * @param id 
     * @returns 
     */
    @UseInterceptors(ClassSerializerInterceptor)
    @Delete(':id')
    @UseGuards(JWTGuard)
    remove(@Param('id') id: string): Promise<User> {
        return this.userService.remove(id);
    }


    /**
    * Upload user profile image 
    * @param id 
    * @param file 
    * @returns file_path
    */
    @Post('/profile-image')
    @UseGuards(JWTGuard)
    @UseInterceptors(FileInterceptor('file', storage))
    async updateFile(@UploadedFile() file) {
        return file.path;
    }

    /**
    * Update User Categories
    * @param id 
    * @param updateUserDto 
    * @returns User
    */
    @UseInterceptors(ClassSerializerInterceptor)
    @Patch('/category/update')
    @UseGuards(JWTGuard)
    updateUserCategories(@Req() request, @Body() updateUserCategoryDto: UpdateUserCategoryDto[]) {
        return this.userService.updateUserCategories(updateUserCategoryDto, request.user);
    }



}
