import { MailerService } from '@nestjs-modules/mailer';
import { Inject, Injectable } from '@nestjs/common';
import { template } from 'handlebars';
import { User } from 'src/models/user.entity';


export interface AuthenticationPayload {
    user: User
    payload: {
        type: string
        access_token?: string
        refresh_token?: string
    }
}

@Injectable()
export class MailService {
    @Inject()
    mailerService: MailerService;

    /**
     * Send email with magic link
     * @param user 
     * @param access_token 
     * @param refresh_token 
     */
    async sendLoginMail(user: User, access_token: string, refresh_token: string) {
        const url = `http://localhost:3000/api/auth?token=${access_token}`

        await this.mailerService.sendMail({
            to: user.email,
            subject: "Welcome to App! Login",
            html: `<p>Hey ${user.firstName} ${user.lastName},</p>
            <p>Please click below to login</p>
            <p>
                <a href="${url}">Login</a>
            </p>
            <p>If you did not request this email you can safely ignore it.</p>`,
            // template: 'confirmation',
            context: {
                name: user.firstName + " " + user.lastName,
                url
            }
        })

        return this.buildResponsePayload(user, access_token, refresh_token);
    }

    buildResponsePayload(user: User, access_token?: string, refresh_token?: string): AuthenticationPayload {
        return {
            user: user,
            payload: {
                type: 'Bearer',
                ...(access_token ? { access_token: access_token } : {}),
                ...(refresh_token ? { refresh_token: refresh_token } : {}),
            }
        }
    }
}
